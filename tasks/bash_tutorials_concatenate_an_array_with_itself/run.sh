#!/bin/bash

N=0
ARRAY=( )

while read LINE
do
    ARRAY+=($LINE)
    N=$((N+1))
done

for ((i=0; i < $N; i+=1))
do
    ARRAY+=(${ARRAY[$i]})
done

for ((i=0; i < $N; i+=1))
do
    ARRAY+=(${ARRAY[$i]})
done

for ((i=0; i < $((3*N)); i+=1))
do
    if [ $i != 0 ]
    then
        printf " "
    fi
    printf ${ARRAY[$i]}
done
echo