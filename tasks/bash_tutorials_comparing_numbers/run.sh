#!/bin/bash

read X
read Y

if [ $X -gt $Y ]
then
    printf "X is greater than Y";
else
    if [ $X -eq $Y ]
    then
        printf "X is equal to Y";
    else
        printf "X is less than Y";
    fi
fi