#!/bin/bash
awk '
BEGIN {
    prev_line = "";
    line_number = 0;
}
{
    if (line_number % 2 != 0) {
        print prev_line ";" $0;
    }
    prev_line = $0;
    ++line_number;
}
END {
    if (line_number % 2 != 0) {
        print $prev_line;
    }
}
'