#!/bin/bash
read N
SUM=0
for ((i=0; i < $N; i+=1))
do
    read VAL
    SUM=$((SUM + VAL))
done
RESULT=$(echo "$SUM / $N" | bc -l)
printf %.3f $RESULT
echo